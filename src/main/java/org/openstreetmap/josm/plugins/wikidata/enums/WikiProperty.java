// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata.enums;

import java.util.Arrays;
import java.util.Map;

import jakarta.json.Json;
import jakarta.json.JsonObjectBuilder;
import jakarta.json.JsonValue;

/**
 * Known Wiki Properties for OSM Wiki Data
 *
 * @author Taylor Smock
 */
public enum WikiProperty implements WikiItemInterface {
    ID, DESCRIPTION, INSTANCE_OF_P2, SUBCLASS_OF_P3, @Deprecated
    IMAGE_P4, STATUS_P6, WIKIDATA_EQUIVALENT_P7, FORMATTER_URL_P8, KEY_TYPE_P9, KEY_FOR_THIS_TAG_P10, PROPOSAL_DISCUSSION_P11, WIKIDATA_CONCEPT_P12, VALUE_VALIDATION_REGEX_P13, PERMANENT_KEY_ID_P16, REDIRECT_TO_P17, DIFFERENT_FROM_P18, PERMANENT_TAG_ID_P19, PROPERTY_DIFFERENT_FROM_P20, RELATION_ROLE_ID_P21, REQUIRED_KEY_OR_TAG_P22, GROUP_P25, LIMITED_TO_LANGUAGE_P26, @Deprecated
    EXCLUDING_REGION_QUALIFIER_P27, IMAGE_P28, MUST_ONLY_BE_USED_IN_REGION_P29, NOT_TO_BE_USED_IN_REGION_P30, DOCUMENTATION_WIKI_PAGES_P31, LANGUAGE_CODE_P32, USE_ON_NODES_P33, USE_ON_WAYS_P34, USE_ON_AREAS_P35, USE_ON_RELATIONS_P36, USE_ON_CHANGESETS_P37, @Deprecated
    RENDERING_IMAGE_P38, OSM_CARTO_IMAGE_P39, TAG_FOR_THIS_RELATION_TYPE_P40, PERMANENT_RELATION_TYPE_ID_P41, BELONGS_TO_RELATION_TYPE_P43, INCOMPATIBLE_WITH_P44, IMPLIES_P45, COMBINATION_P46, IMAGE_CAPTION_P47, LIMITED_TO_REGION_P48, GEOGRAPHIC_CODE_P49, REDIRECTS_TO_WIKI_PAGE_P50, IDENTICAL_TO_P51;

    /**
     * Wiki properties that are strings
     */
    private static final WikiProperty[] STRING_TYPES = { DESCRIPTION, FORMATTER_URL_P8, KEY_FOR_THIS_TAG_P10,
            VALUE_VALIDATION_REGEX_P13, PERMANENT_KEY_ID_P16, PERMANENT_TAG_ID_P19, IMAGE_P28,
            DOCUMENTATION_WIKI_PAGES_P31, LANGUAGE_CODE_P32, OSM_CARTO_IMAGE_P39, PERMANENT_RELATION_TYPE_ID_P41,
            IMAGE_CAPTION_P47, GEOGRAPHIC_CODE_P49 };

    /**
     * Wiki properties that are strings for specific languages
     */
    private static final WikiProperty[] MONOLINGUAL_TYPES = { WikiProperty.DOCUMENTATION_WIKI_PAGES_P31 };
    private final String pid;
    private final String name;

    WikiProperty() {
        final String[] enumName = this.name().split("_", -1);
        this.pid = enumName.length >= 2 ? enumName[enumName.length - 1] : "";
        this.name = this.name().replace("_" + pid, "").replace("_", " ");
    }

    /**
     * Get the value from a map where the enum is a key. This is short for
     * {@code map.get(enum.name())}
     *
     * @param <T>
     *            The return type of the map
     * @param map
     *            The map to get the enum from (should not be null)
     * @return The value in the map
     */

    public <T> T get(final Map<String, T> map) {
        final T defaultReturn = map.get(this.getId());
        if (defaultReturn != null) {
            return defaultReturn;
        }
        return map.get(this.getDescriptor());
    }

    /**
     * Convert an entry to a snak
     * @param object The object to convert
     * @return The snak
     */
    public JsonValue toWikiSnak(Object object) {
        JsonObjectBuilder mainSnak;
        if (object instanceof WikiItemInterface) {
            WikiItemInterface item = (WikiItemInterface) object;
            JsonObjectBuilder value = Json.createObjectBuilder();
            value.add("entity-type", "item");
            value.add("numeric-id", item.getId().replaceFirst("Q", ""));
            value.add("id", item.getId());
            JsonObjectBuilder dataValue = Json.createObjectBuilder().add("value", value);
            dataValue.add("type", "wikibase-entityid");
            mainSnak = Json.createObjectBuilder().add("datavalue", dataValue);
        } else if (object instanceof String && Arrays.asList(MONOLINGUAL_TYPES).contains(this)) {
            JsonObjectBuilder value = Json.createObjectBuilder();
            value.add("text", object.toString());
            value.add("language", "en"); // TODO currently assuming English
            JsonObjectBuilder dataValue = Json.createObjectBuilder();
            dataValue.add("value", value);
            dataValue.add("type", "monolingualtext");
            mainSnak = Json.createObjectBuilder().add("datavalue", dataValue);
        } else if (object instanceof String && Arrays.asList(STRING_TYPES).contains(this)) {
            JsonObjectBuilder dataValue = Json.createObjectBuilder();
            dataValue.add("type", "string");
            dataValue.add("value", object.toString());
            mainSnak = Json.createObjectBuilder().add("datavalue", dataValue);
        } else {
            throw new UnsupportedOperationException();
        }
        mainSnak.add("property", this.pid);
        mainSnak.add("snaktype", "value");
        JsonObjectBuilder claim = Json.createObjectBuilder().add("mainsnak", mainSnak.build());
        claim.add("type", "statement");
        //claim.add("rank", "normal");
        return claim.build();
    }

    @Override
    public String getDescriptor() {
        return this.name;
    }

    @Override
    public String getId() {
        return this.pid;
    }

    /**
     * Check if the values for this key should <i>always</i> be a {@link String}
     * @return {@code true} if the value should be a string
     */
    public boolean isString() {
        return this == PERMANENT_KEY_ID_P16 || this == PERMANENT_TAG_ID_P19 || this == PERMANENT_RELATION_TYPE_ID_P41;
    }
}
