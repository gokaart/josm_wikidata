// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata.enums;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;

/**
 * Document unstable methods
 * @author Taylor Smock
 *
 */
@Documented
@Retention(RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
public @interface Unstable {

}
