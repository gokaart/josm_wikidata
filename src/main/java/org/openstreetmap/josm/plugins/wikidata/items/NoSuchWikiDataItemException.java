// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata.items;

import static org.openstreetmap.josm.tools.I18n.tr;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NoSuchWikiDataItemException extends Exception {
    private static final long serialVersionUID = 3041891623312582502L;

    public NoSuchWikiDataItemException(String key, String value) {
        super(tr("No WikiData for {0}", Stream.of(key, value).filter(Objects::nonNull)
                .filter(string -> !string.trim().isEmpty()).collect(Collectors.joining("="))));
    }
}
