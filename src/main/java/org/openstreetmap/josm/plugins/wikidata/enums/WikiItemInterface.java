// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata.enums;

/**
 * An interface with common wiki item and wiki property values
 *
 * @author Taylor Smock
 */
public interface WikiItemInterface {
    /**
     * The descriptor for the wiki item
     * @return The base description for the wiki item
     */
    String getDescriptor();

    /**
     * The ID for the wiki item
     * @return The unique ID for the wiki item
     */
    String getId();
}
