// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata.io.upload;

import static org.openstreetmap.josm.tools.I18n.tr;

import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator.RequestorType;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JOptionPane;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.cookie.BasicCookieStore;
import org.apache.hc.client5.http.entity.mime.FormBodyPartBuilder;
import org.apache.hc.client5.http.entity.mime.MultipartEntityBuilder;
import org.apache.hc.client5.http.entity.mime.StringBody;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.Notification;
import org.openstreetmap.josm.gui.util.GuiHelper;
import org.openstreetmap.josm.gui.widgets.HtmlPanel;
import org.openstreetmap.josm.io.auth.CredentialsAgentException;
import org.openstreetmap.josm.io.auth.CredentialsAgentResponse;
import org.openstreetmap.josm.io.auth.CredentialsManager;
import org.openstreetmap.josm.plugins.wikidata.enums.WikiDataItem;
import org.openstreetmap.josm.plugins.wikidata.enums.WikiProperty;
import org.openstreetmap.josm.plugins.wikidata.io.download.WikiDataItemGetter;
import org.openstreetmap.josm.plugins.wikidata.items.DataItem;
import org.openstreetmap.josm.plugins.wikidata.items.NoSuchWikiDataItemException;
import org.openstreetmap.josm.spi.preferences.Config;
import org.openstreetmap.josm.tools.ImageProvider;
import org.openstreetmap.josm.tools.JosmRuntimeException;
import org.openstreetmap.josm.tools.Logging;
import org.openstreetmap.josm.tools.bugreport.BugReport;
import org.openstreetmap.josm.tools.bugreport.ReportedException;

import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonArrayBuilder;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;
import jakarta.json.JsonReader;

public class WikiDataItemUploader implements Runnable {

    private static final String BASE_URL = "https://wiki.openstreetmap.org/w/api.php";
    private static final String LOGIN_TOKEN = "?action=query&meta=tokens&type=login";
    private static boolean lastLoginFailed;

    private final Map<WikiProperty, Object> item;

    public WikiDataItemUploader(Map<WikiProperty, Object> item) {
        this.item = item;
    }

    @Override
    public void run() {
        Map<WikiProperty, Object> toUpload = this.item;
        String tag = toUpload.getOrDefault(WikiProperty.PERMANENT_TAG_ID_P19, "").toString();
        String key = toUpload.getOrDefault(WikiProperty.PERMANENT_KEY_ID_P16, "").toString();
        if ((tag == null || tag.trim().isEmpty()) && (key == null || key.trim().isEmpty())) {
            throw new JosmRuntimeException("P19 and P16 cannot both be null");
        }
        String value = null;
        if (tag != null && (key == null || key.isEmpty())) {
            String[] parts = tag.split("=", -1);
            key = parts[0];
            value = parts[1];
        }
        try {
            // TODO for editing the old item
            DataItem oldItem = WikiDataItemGetter.getDataItem(key, value);
        } catch (IOException e) {
            throw new JosmRuntimeException(e);
        } catch (NoSuchWikiDataItemException e) {
            // Pass. We are OK with that.
            Logging.trace(e);
        }

        try {
            doUpload(queryPart(toUpload));
            WikiDataItemGetter.getDataItem(key, value, WikiDataItemGetter.Options.FORCE_DOWNLOAD);
        } catch (IOException | NoTokenException e) {
            throw new JosmRuntimeException(e);
        } catch (NoSuchWikiDataItemException e) {
            // Pass. We are OK with that.
            Logging.trace(e);
        }
    }

    private static void doUpload(String toUpload) throws NoTokenException {
        CredentialsAgentResponse credentials;
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            credentials = CredentialsManager.getInstance().getCredentials(RequestorType.SERVER,
                    "https://wiki.openstreetmap.org", lastLoginFailed);
            String lToken;
            HttpClientContext context = HttpClientContext.create();
            context.setCookieStore(new BasicCookieStore());
            try (CloseableHttpResponse tokenResponse = httpClient
                    .execute(new HttpGet(BASE_URL + LOGIN_TOKEN + "&format=json"), context);
                    InputStream tokenStream = tokenResponse.getEntity().getContent();
                    JsonReader reader = Json.createReader(tokenStream)) {
                JsonObject obj = reader.readObject().getJsonObject("query").getJsonObject("tokens");
                lToken = obj.getJsonString("logintoken").getString();
            }
            HttpPost post = new HttpPost(BASE_URL);
            MultipartEntityBuilder queryBuilder = MultipartEntityBuilder.create();
            Map<String, String> queryParts = new TreeMap<>();
            queryParts.put("action", "clientlogin");
            queryParts.put("format", "json");
            queryParts.put("loginreturnurl", "http://example.org");
            queryParts.put("logintoken", lToken);
            queryParts.put("username", credentials.getUsername());
            queryParts.put("password", new String(credentials.getPassword()));
            for (Map.Entry<String, String> entry : queryParts.entrySet()) {
                queryBuilder.addPart(FormBodyPartBuilder
                        .create(entry.getKey(), new StringBody(entry.getValue(), ContentType.MULTIPART_FORM_DATA))
                        .build());
            }
            post.setEntity(queryBuilder.build());
            try (CloseableHttpResponse loginResponse = httpClient.execute(post, context);
                    InputStream loginStream = loginResponse.getEntity().getContent();
                    JsonReader reader = Json.createReader(loginStream)) {
                JsonObject resultObject = reader.readObject();
                String status = resultObject.getJsonObject("clientlogin").getString("status");
                if (!"PASS".equals(status)) {
                    lastLoginFailed = true;
                    int option = GuiHelper.runInEDTAndWaitAndReturn(() -> JOptionPane
                            .showConfirmDialog(MainApplication.getMainFrame(), tr("Login failed. Try again?")));
                    if (option == JOptionPane.YES_OPTION) {
                        doUpload(toUpload);
                    }
                    return;
                }
                lastLoginFailed = false;
            }
            String csrfUrl = BASE_URL + LOGIN_TOKEN.replace("login", "csrf") + "&format=json";
            HttpGet csrfGet = new HttpGet(csrfUrl);
            String csrfToken;
            try (CloseableHttpResponse csrfResponse = httpClient.execute(csrfGet, context);
                    InputStream csrfStream = csrfResponse.getEntity().getContent();
                    JsonReader reader = Json.createReader(csrfStream)) {
                JsonObject obj = reader.readObject().getJsonObject("query").getJsonObject("tokens");
                csrfToken = obj.getJsonString("csrftoken").getString();
            }
            HttpPost upload = new HttpPost(BASE_URL);
            queryParts.clear();
            queryParts.put("action", "wbeditentity");
            queryParts.put("format", "json");
            queryParts.put("new", "item");
            queryParts.put("data", toUpload);
            queryParts.put("token", csrfToken);
            queryBuilder = MultipartEntityBuilder.create();
            for (Map.Entry<String, String> entry : queryParts.entrySet()) {
                queryBuilder.addPart(FormBodyPartBuilder
                        .create(entry.getKey(), new StringBody(entry.getValue(), ContentType.MULTIPART_FORM_DATA))
                        .build());
            }
            upload.setEntity(queryBuilder.build());
            try {
                Logging.info(EntityUtils.toString(upload.getEntity()));
            } catch (ParseException e) {
                Logging.error(e);
            }
            try (CloseableHttpResponse uploadResponse = httpClient.execute(upload, context);
                    InputStream uploadStream = uploadResponse.getEntity().getContent();
                    JsonReader uploadReader = Json.createReader(uploadStream)) {
                JsonObject object = uploadReader.readObject();
                Logging.info(object.toString());
                Notification notification = new Notification();
                String id = object.containsKey("entity") ? object.getJsonObject("entity").getString("id") : null;
                GuiHelper.runInEDTAndWait(() -> {
                    final HtmlPanel panel;
                    if (id != null) {
                        panel = new HtmlPanel("<h3><a href=\"" + Config.getUrls().getOSMWiki() + "/wiki/Item:" + id
                                + "\">" + tr("Wiki Data Item upload successful") + "</a></h3>");
                    } else {
                        final ReportedException reportedException = BugReport
                                .intercept(new IOException("OSM Wiki didn't return an expected response"));
                        reportedException.startSection("WikiData Response");
                        reportedException.put("data", toUpload);
                        throw reportedException;
                    }
                    panel.enableClickableHyperlinks();
                    panel.setOpaque(false);
                    notification.setContent(panel);
                    notification.setIcon(ImageProvider.get("misc", "check_large"));
                    notification.show();
                });
            }
        } catch (CredentialsAgentException | IOException e) {
            Logging.error(e);
            throw new NoTokenException("Upload failed", e);
        }
    }

    public static String queryPart(Map<WikiProperty, Object> toUpload) {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        JsonObjectBuilder labels = Json.createObjectBuilder();
        Map<String, Object> labelMap = new HashMap<>();
        labelMap.put("language", "en");
        labelMap.put("value", toUpload.getOrDefault(WikiProperty.PERMANENT_TAG_ID_P19,
                toUpload.get(WikiProperty.PERMANENT_KEY_ID_P16)));
        labels.add("en", Json.createObjectBuilder(labelMap));
        builder.add("labels", labels);

        String description = toUpload.getOrDefault(WikiProperty.DESCRIPTION, "").toString();
        toUpload.remove(WikiProperty.DESCRIPTION);
        if (!description.isEmpty()) {
            JsonObjectBuilder descriptions = Json.createObjectBuilder();
            descriptions.add("language", "en");
            descriptions.add("value", description);
            builder.add("descriptions", Json.createObjectBuilder().add("en", descriptions));
        }

        JsonArrayBuilder claims = Json.createArrayBuilder();
        final String title;
        if (toUpload.containsKey(WikiProperty.PERMANENT_KEY_ID_P16)) {
            claims.add(WikiProperty.INSTANCE_OF_P2.toWikiSnak(WikiDataItem.KEY_Q7));
            title = "Key:" + toUpload.get(WikiProperty.PERMANENT_KEY_ID_P16).toString();
        } else if (toUpload.containsKey(WikiProperty.PERMANENT_TAG_ID_P19)) {
            claims.add(WikiProperty.INSTANCE_OF_P2.toWikiSnak(WikiDataItem.TAG_Q2));
            title = "Tag:" + toUpload.get(WikiProperty.PERMANENT_TAG_ID_P19).toString();
        } else {
            title = null;
        }

        toUpload.computeIfPresent(WikiProperty.KEY_FOR_THIS_TAG_P10, (WikiProperty p, Object obj) -> {
            if (!(obj instanceof String)) {
                return obj;
            }
            try {
                return WikiDataItemGetter.getDataItem((String) obj, null);
            } catch (IOException | NoSuchWikiDataItemException e) {
                final ReportedException reportedException = BugReport.intercept(e);
                reportedException.startSection("Wikidata");
                reportedException.put("Data Item: ", obj);
                reportedException.warn();
                return obj;
            }
        });

        if (title != null) {
            toUpload.putIfAbsent(WikiProperty.DOCUMENTATION_WIKI_PAGES_P31, title);
            JsonObjectBuilder wiki = Json.createObjectBuilder();
            wiki.add("site", "wiki");
            wiki.add("title", title);
            builder.add("sitelinks", Json.createObjectBuilder().add("wiki", wiki));
        }

        for (Map.Entry<WikiProperty, Object> entry : toUpload.entrySet()) {
            try {
                claims.add(entry.getKey().toWikiSnak(entry.getValue()));
            } catch (UnsupportedOperationException e) {
                Logging.error(e);
                Logging.info("{0} cannot convert {1}", entry.getKey(), entry.getValue());
            }
        }

        // JsonObjectBuilder#build clears out the builder
        JsonArray claimObject = claims.build();
        if (!claimObject.isEmpty()) {
            builder.add("claims", claimObject);
        }
        return builder.build().toString();
    }
}
