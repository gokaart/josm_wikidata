// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata.gui.dialog;

import static org.openstreetmap.josm.tools.I18n.tr;
import java.awt.GridBagLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.EnumMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

import org.openstreetmap.josm.gui.ExtendedDialog;
import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.widgets.JosmTextField;
import org.openstreetmap.josm.gui.widgets.QuadStateCheckBox;
import org.openstreetmap.josm.plugins.wikidata.enums.WikiDataItem;
import org.openstreetmap.josm.plugins.wikidata.enums.WikiProperty;
import org.openstreetmap.josm.plugins.wikidata.io.upload.WikiDataItemUploader;
import org.openstreetmap.josm.tools.GBC;

public class WikiDataEditing extends ExtendedDialog {

    public WikiDataEditing(String title, Map<WikiProperty, Object> initialValues) {
        super(MainApplication.getMainFrame(), title, new String[] { tr("Upload"), tr("Cancel") }, true, true);
        // Default to cancel. For now.
        setDefaultButton(2);
        JPanel content = new JPanel(new GridBagLayout());
        JosmTextField keyField = new JosmTextField();
        keyField.setHint(tr("key"));
        Object keyObject = initialValues.getOrDefault(WikiProperty.PERMANENT_KEY_ID_P16,
                initialValues.getOrDefault(WikiProperty.KEY_FOR_THIS_TAG_P10, ""));
        String key = "";
        if (keyObject instanceof String) {
            key = (String) keyObject;
            keyField.setText(key);
        }
        JosmTextField valueField = new JosmTextField();
        valueField.setHint(tr("value"));
        JosmTextField descriptionField = new JosmTextField();
        descriptionField.setHint(tr("Description"));
        Object tagObject = initialValues.get(WikiProperty.PERMANENT_TAG_ID_P19);
        String value = "";
        if (tagObject instanceof String) {
            value = ((String) tagObject).split("=", -1)[1];
            valueField.setText(value);
        }
        Object descriptionObject = initialValues.get(WikiProperty.DESCRIPTION);
        String description = "";
        if (descriptionObject instanceof String) {
            description = (String) descriptionObject;
            descriptionField.setText(description);
        }
        content.add(keyField, GBC.std().fill(GBC.HORIZONTAL));
        content.add(valueField, GBC.eol().fill(GBC.HORIZONTAL));
        content.add(descriptionField, GBC.eol().fill(GBC.HORIZONTAL));
        JCheckBox wellKnown = new JCheckBox(tr("Well known"));
        wellKnown.setToolTipText(tr("Well Known"));
        wellKnown.setEnabled(valueField.getText().trim().isEmpty());
        content.add(wellKnown, GBC.eol().fill(GBC.HORIZONTAL));

        JosmTextField regex = new JosmTextField();
        regex.setHint(tr("Value Validation Regex"));
        regex.setEnabled(valueField.getText().trim().isEmpty());
        wellKnown.addActionListener(l -> regex.setEnabled(!wellKnown.isSelected()));
        content.add(regex, GBC.eol().fill(GBC.HORIZONTAL));

        valueField.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                // Do nothing
            }

            @Override
            public void focusLost(FocusEvent e) {
                boolean blank = valueField.getText().trim().isEmpty();
                wellKnown.setEnabled(blank);
                regex.setEnabled(blank);
            }
        });

        descriptionField.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
                // The maximum length of a field is 250 characters. Just about everything else will always be below 250, but
                // the description can get a bit long.
                if (descriptionField.getText().trim().length() > 250) {
                    descriptionField.setText(descriptionField.getText().trim().substring(0, 249));
                }
            }

            @Override
            public void keyPressed(KeyEvent e) {
                // Don't care
            }

            @Override
            public void keyReleased(KeyEvent e) {
                // Don't care
            }
        });

        QuadStateCheckBox.State initial = QuadStateCheckBox.State.UNSET;
        QuadStateCheckBox.State[] allowedStates = { QuadStateCheckBox.State.NOT_SELECTED,
                QuadStateCheckBox.State.SELECTED, QuadStateCheckBox.State.UNSET };
        BiFunction<WikiProperty, Map<WikiProperty, Object>, QuadStateCheckBox.State> initialStateFunction = (p, m) -> {
            if (m.containsKey(p)) {
                Object mapValue = m.get(p);
                if (mapValue instanceof Object[]) {
                    mapValue = ((Object[]) mapValue)[0];
                }
                if (WikiDataItem.IS_ALLOWED_Q8000 == mapValue) {
                    return QuadStateCheckBox.State.SELECTED;
                } else if (WikiDataItem.IS_PROHIBITED_Q8001 == mapValue) {
                    return QuadStateCheckBox.State.NOT_SELECTED;
                }
            }
            return initial;
        };
        QuadStateCheckBox useOnNodes = new QuadStateCheckBox(tr("Use on nodes"),
                initialStateFunction.apply(WikiProperty.USE_ON_NODES_P33, initialValues), allowedStates);
        QuadStateCheckBox useOnWays = new QuadStateCheckBox(tr("Use on ways"),
                initialStateFunction.apply(WikiProperty.USE_ON_WAYS_P34, initialValues), allowedStates);
        QuadStateCheckBox useOnAreas = new QuadStateCheckBox(tr("Use on areas"),
                initialStateFunction.apply(WikiProperty.USE_ON_AREAS_P35, initialValues), allowedStates);
        QuadStateCheckBox useOnRelations = new QuadStateCheckBox(tr("Use on relations"),
                initialStateFunction.apply(WikiProperty.USE_ON_RELATIONS_P36, initialValues), allowedStates);
        QuadStateCheckBox useOnChangesets = new QuadStateCheckBox(tr("Use on changesets"),
                initialStateFunction.apply(WikiProperty.USE_ON_CHANGESETS_P37, initialValues), allowedStates);
        content.add(useOnNodes, GBC.std());
        content.add(useOnWays, GBC.std());
        content.add(useOnAreas, GBC.std());
        content.add(useOnRelations, GBC.std());
        content.add(useOnChangesets, GBC.eol());

        setContent(content);

        super.showDialog();
        int selected = super.getValue();
        if (selected == 1 && !keyField.getText().trim().isEmpty()) {
            key = keyField.getText().trim();
            value = valueField.getText().trim();
            description = descriptionField.getText().trim();
            EnumMap<WikiProperty, Object> map = new EnumMap<>(WikiProperty.class);
            if (value.isEmpty()) {
                map.put(WikiProperty.PERMANENT_KEY_ID_P16, key);
                if (!regex.getText().trim().isEmpty()) {
                    map.put(WikiProperty.VALUE_VALIDATION_REGEX_P13, regex.getText().trim());
                }
            } else {
                map.put(WikiProperty.PERMANENT_TAG_ID_P19, String.join("=", key, value));
                map.put(WikiProperty.KEY_FOR_THIS_TAG_P10, key);
            }
            if (!description.isEmpty()) {
                map.put(WikiProperty.DESCRIPTION, description);
            }
            QuadStateCheckBox.State useOnSelected = QuadStateCheckBox.State.SELECTED;
            Function<Boolean, WikiDataItem> getQ800x = bool -> Boolean.TRUE.equals(bool) ? WikiDataItem.IS_ALLOWED_Q8000
                    : WikiDataItem.IS_PROHIBITED_Q8001;
            if (useOnNodes.getState() != initial) {
                map.put(WikiProperty.USE_ON_NODES_P33, getQ800x.apply(useOnSelected == useOnNodes.getState()));
            }
            if (useOnWays.getState() != initial) {
                map.put(WikiProperty.USE_ON_WAYS_P34, getQ800x.apply(useOnSelected == useOnWays.getState()));
            }
            if (useOnAreas.getState() != initial) {
                map.put(WikiProperty.USE_ON_AREAS_P35, getQ800x.apply(useOnSelected == useOnAreas.getState()));
            }
            if (useOnRelations.getState() != initial) {
                map.put(WikiProperty.USE_ON_RELATIONS_P36, getQ800x.apply(useOnSelected == useOnRelations.getState()));
            }
            if (useOnChangesets.getState() != initial) {
                map.put(WikiProperty.USE_ON_CHANGESETS_P37,
                        getQ800x.apply(useOnSelected == useOnChangesets.getState()));
            }

            MainApplication.worker.execute(new WikiDataItemUploader(map));
        }
    }

}
