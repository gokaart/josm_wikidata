// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata;

import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.AbstractAction;
import javax.swing.JPopupMenu;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import org.openstreetmap.josm.gui.dialogs.properties.PropertiesDialog;
import org.openstreetmap.josm.plugins.wikidata.enums.WikiDataItem;
import org.openstreetmap.josm.plugins.wikidata.enums.WikiProperty;
import org.openstreetmap.josm.plugins.wikidata.gui.dialog.WikiDataEditing;
import org.openstreetmap.josm.plugins.wikidata.io.download.WikiDataItemGetter;
import org.openstreetmap.josm.plugins.wikidata.items.DataItem;
import org.openstreetmap.josm.plugins.wikidata.items.NoSuchWikiDataItemException;
import org.openstreetmap.josm.tools.Destroyable;
import org.openstreetmap.josm.tools.Logging;
import org.openstreetmap.josm.tools.OpenBrowser;

public class KeyListener implements PopupMenuListener, Destroyable {
    private JPopupMenu menu;
    private PropertiesDialog properties;
    private final Map<JPopupMenu, List<String>> addedValues = new HashMap<>();
    private final Map<JPopupMenu, List<Component>> addedComponents = new HashMap<>();

    public KeyListener(PropertiesDialog properties, JPopupMenu menu) {
        this.menu = menu;
        this.properties = properties;
        menu.addPopupMenuListener(this);
    }

    @Override
    public void destroy() {
        if (menu != null) {
            menu.removePopupMenuListener(this);
        }
        properties = null;
        menu = null;
    }

    @Override
    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        JPopupMenu popup = (JPopupMenu) e.getSource();
        properties.visitSelectedProperties((primitive, key, value) -> addAction(popup, key, value));
    }

    @Override
    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        JPopupMenu popup = (JPopupMenu) e.getSource();
        if (addedValues.containsKey(popup) && addedComponents.containsKey(popup)) {
            addedValues.remove(popup);
            addedComponents.get(popup).forEach(popup::remove);
            addedComponents.remove(popup);
        }
    }

    @Override
    public void popupMenuCanceled(PopupMenuEvent e) {
        // Do nothing
    }

    private void addAction(JPopupMenu popup, String key, String value) {
        if (value == null || value.trim().isEmpty()) {
            return;
        }
        List<String> strings = addedValues.computeIfAbsent(popup, p -> new ArrayList<>());
        List<Component> components = addedComponents.computeIfAbsent(popup, p -> new ArrayList<>());
        final String tag = String.join("=", key, value);
        if (!strings.contains(tag)) {
            strings.add(tag);
            try {
                WikiDataItemAction current = null;
                try {
                    current = new WikiDataItemAction(key, value);
                    components.add(popup.add(current));
                    if (current.dataItem.getDataMap().containsKey(WikiProperty.PERMANENT_KEY_ID_P16)
                            && (current.dataItem.getDataMap().containsKey(WikiProperty.KEY_TYPE_P9)
                                    && WikiDataItem.WELL_KNOWN_VALUES_Q8
                                            .equals(current.dataItem.getDataMap().get(WikiProperty.KEY_TYPE_P9))
                                    || !current.dataItem.getDataMap()
                                            .containsKey(WikiProperty.VALUE_VALIDATION_REGEX_P13))) {
                        components.add(popup.add(new NewWikiDataItemAction(key, value)));
                    }
                } catch (NoSuchWikiDataItemException e) {
                    components.add(popup.add(new NewWikiDataItemAction(key, null)));
                }
            } catch (IOException e) {
                Logging.error(e);
                strings.remove(tag);
            }
        }
    }

    private static class WikiDataItemAction extends AbstractAction {
        /** The data item used */
        public final DataItem dataItem;

        public WikiDataItemAction(final String key, final String value)
                throws IOException, NoSuchWikiDataItemException {
            this.dataItem = getWikiItem(key, value);
            putValue(NAME, tr("Open WikiData for {0} ({1})", this.dataItem.getDescriptor(), this.dataItem.getId()));

        }

        @Override
        public void actionPerformed(ActionEvent e) {
            OpenBrowser.displayUrl("https://wiki.osm.org/wiki/Item:" + this.dataItem.getId());
        }

        private static DataItem getWikiItem(final String key, final String value)
                throws IOException, NoSuchWikiDataItemException {
            try {
                return WikiDataItemGetter.getDataItem(key, value);
            } catch (NoSuchWikiDataItemException e) {
                return WikiDataItemGetter.getDataItem(key, null);
            }
        }
    }

    private static class NewWikiDataItemAction extends AbstractAction {
        private final String key;
        private final String value;

        NewWikiDataItemAction(final String key, final String value) {
            this.key = key;
            this.value = value;
            putValue(NAME, tr("Create new WikiData item for {0}",
                    Stream.of(this.key, this.value).filter(Objects::nonNull).collect(Collectors.joining("="))));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            EnumMap<WikiProperty, Object> map = new EnumMap<>(WikiProperty.class);
            final String title;
            if (this.value != null && !this.value.trim().isEmpty()) {
                title = String.join("=", this.key, this.value);
                map.put(WikiProperty.PERMANENT_TAG_ID_P19, title);
                map.put(WikiProperty.KEY_FOR_THIS_TAG_P10, this.key);
            } else {
                title = this.key;
                map.put(WikiProperty.PERMANENT_KEY_ID_P16, title);
            }
            // Prefill some data, if possible
            if (this.value != null) {
                try {
                    DataItem dataItem = WikiDataItemGetter.getDataItem(this.key, null);
                    for (WikiProperty p : Arrays.asList(WikiProperty.USE_ON_NODES_P33, WikiProperty.USE_ON_WAYS_P34,
                            WikiProperty.USE_ON_AREAS_P35, WikiProperty.USE_ON_RELATIONS_P36,
                            WikiProperty.USE_ON_CHANGESETS_P37)) {
                        if (dataItem.getDataMap().containsKey(p)) {
                            map.put(p, dataItem.getDataMap().get(p));
                        }
                    }
                } catch (NoSuchWikiDataItemException | IOException exception) {
                    Logging.trace(exception);
                }
            }
            new WikiDataEditing(tr("Create WikiData item for {0}", title), map);
        }
    }
}
