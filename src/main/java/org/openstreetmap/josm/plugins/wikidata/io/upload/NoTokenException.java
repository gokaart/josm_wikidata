// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata.io.upload;

public class NoTokenException extends Exception {
    public NoTokenException(String str) {
        super(str);
    }

    public NoTokenException(String string, Exception e) {
        super(string, e);
    }
}
