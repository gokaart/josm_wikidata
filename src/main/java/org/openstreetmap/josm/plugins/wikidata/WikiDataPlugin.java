// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashSet;

import javax.swing.JPopupMenu;

import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.MapFrame;
import org.openstreetmap.josm.gui.dialogs.properties.PropertiesDialog;
import org.openstreetmap.josm.plugins.Plugin;
import org.openstreetmap.josm.plugins.PluginInformation;
import org.openstreetmap.josm.tools.Destroyable;
import org.openstreetmap.josm.tools.Logging;

public final class WikiDataPlugin extends Plugin implements Destroyable {
    private final Collection<Destroyable> destroyables = new HashSet<>();
    private KeyListener popupHandler;

    public WikiDataPlugin(PluginInformation info) {
        super(info);
    }

    @Override
    public void mapFrameInitialized(MapFrame oldFrame, MapFrame newFrame) {
        if (oldFrame == null && newFrame != null) { // map frame added
            try {
                PropertiesDialog properties = newFrame.propertiesDialog;

                Field tagMenuField = PropertiesDialog.class.getDeclaredField("tagMenu");
                tagMenuField.setAccessible(true);
                JPopupMenu tagMenu = (JPopupMenu) tagMenuField.get(properties);
                popupHandler = new KeyListener(properties, tagMenu);
                this.destroyables.add(popupHandler);
            } catch (ReflectiveOperationException e) {
                Logging.debug(e);
            }
        } else if (oldFrame != null && newFrame == null) { // map frame removed
            if (this.popupHandler != null) {
                this.destroyables.remove(this.popupHandler);
                this.popupHandler.destroy();
                this.popupHandler = null;
            }
        }
    }

    @Override
    public void destroy() {
        this.mapFrameInitialized(MainApplication.getMap(), null);
    }
}
