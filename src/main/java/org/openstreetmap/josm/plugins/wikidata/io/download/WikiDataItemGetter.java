// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata.io.download;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;

import org.openstreetmap.josm.io.CachedFile;
import org.openstreetmap.josm.plugins.wikidata.items.DataItem;
import org.openstreetmap.josm.plugins.wikidata.items.NoSuchWikiDataItemException;
import org.openstreetmap.josm.tools.Logging;

public final class WikiDataItemGetter {
    public enum Options {
        FORCE_DOWNLOAD
    }

    /** The URL to get data from (use {@link MessageFormat#format}) */
    public static final String URL = "https://wiki.openstreetmap.org/w/api.php?action=wbgetentities&sites=wiki&format=json&formatversion=2&titles={0}";

    public static DataItem getDataItem(String key, String value, Options... options)
            throws IOException, NoSuchWikiDataItemException {
        Objects.requireNonNull(key, "key should not be null");
        String[] toGet = new String[2];
        toGet[1] = "Key:" + key;
        if (value != null && !value.trim().isEmpty()) {
            // Try tag first
            toGet[0] = "Tag:" + key + "=" + value;
        }
        toGet = Stream.of(toGet).filter(Objects::nonNull).map(str -> str.replace(' ', '_').trim())
                .toArray(String[]::new);
        for (String string : toGet) {
            boolean force = options != null && Arrays.asList(options).contains(Options.FORCE_DOWNLOAD);
            if (force) {
                try (CachedFile file = new CachedFile(MessageFormat.format(URL, string))) {
                    file.clear();
                }
            }
            try (CachedFile file = new CachedFile(MessageFormat.format(URL, string));
                    InputStream inputStream = file.getInputStream();
                    JsonReader reader = Json.createReader(inputStream)) {
                JsonObject object = reader.readObject();
                if (object.containsKey("entities")) {
                    JsonObject entities = object.getJsonObject("entities");
                    // If there are no "real" entities, fall back to no such wiki data item
                    if (!(entities.keySet().size() == 1 && entities.containsKey("-1"))) {
                        Optional<DataItem> first = entities.values().stream().filter(JsonObject.class::isInstance)
                                .map(JsonObject.class::cast).map(DataItem::new).findFirst();
                        if (first.isPresent()) {
                            return first.get();
                        }
                    }
                }
            } catch (IOException e) {
                Logging.trace(e);
            }
        }
        throw new NoSuchWikiDataItemException(key, value);
    }

    private WikiDataItemGetter() {
        // Hide constructor
    }
}
