// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata.items;

import java.io.Serializable;
import java.util.EnumMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Stream;

import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonValue;
import jakarta.json.JsonValue.ValueType;

import org.openstreetmap.josm.plugins.wikidata.enums.WikiDataItem;
import org.openstreetmap.josm.plugins.wikidata.enums.WikiItemInterface;
import org.openstreetmap.josm.plugins.wikidata.enums.WikiProperty;
import org.openstreetmap.josm.tools.LanguageInfo;
import org.openstreetmap.josm.tools.Logging;

public class DataItem implements WikiItemInterface, Serializable {
    private final EnumMap<WikiProperty, Object> dataMap = new EnumMap<>(WikiProperty.class);
    private final String id;
    private final String baseName;
    private final String languageCode = LanguageInfo.getWikiLanguagePrefix();
    private final String label;
    private final String description;
    private final JsonObject originalData;

    public DataItem(JsonObject jsonData) {
        this.id = jsonData.getString("id");
        this.originalData = jsonData;
        String claimString = "claims";
        if (jsonData.containsKey(claimString) && jsonData.get(claimString).getValueType() == ValueType.OBJECT) {
            JsonObject claims = jsonData.getJsonObject(claimString);
            for (Map.Entry<String, JsonValue> claim : claims.entrySet()) {
                WikiProperty property = Stream.of(WikiProperty.values())
                        .filter(p -> claim.getKey().equalsIgnoreCase(p.getId())).findAny().orElse(null);
                if (property != null) {
                    dataMap.put(property, parseSnak(claim.getValue()));
                }
            }
        }
        this.label = getLocalizedField("labels", jsonData);
        this.description = getLocalizedField("description", jsonData);
        updateProperties();
        if (dataMap.containsKey(WikiProperty.PERMANENT_TAG_ID_P19)) {
            this.baseName = (String) dataMap.get(WikiProperty.PERMANENT_TAG_ID_P19);
        } else if (dataMap.containsKey(WikiProperty.PERMANENT_KEY_ID_P16)) {
            this.baseName = (String) dataMap.get(WikiProperty.PERMANENT_KEY_ID_P16);
        } else if (dataMap.containsKey(WikiProperty.PERMANENT_RELATION_TYPE_ID_P41)) {
            this.baseName = (String) dataMap.get(WikiProperty.PERMANENT_RELATION_TYPE_ID_P41);
        } else {
            this.baseName = null;
        }
    }

    /**
     * Get the label for the current language
     * @param field The field to get a localized value for
     * @param jsonData The jsonData with a value field
     * @return The value for the current language, or a default value (whichever value is first, most likely English).
     */
    private String getLocalizedField(String field, JsonObject jsonData) {
        if (jsonData.containsKey(field) && jsonData.get(field).getValueType() == ValueType.OBJECT) {
            JsonObject labels = jsonData.getJsonObject(field);
            if (labels.containsKey(languageCode)) {
                return labels.getJsonObject(languageCode).getString("value");
            } else {
                for (Map.Entry<String, JsonValue> entry : labels.entrySet()) {
                    if (entry.getValue().getValueType() == ValueType.OBJECT) {
                        return entry.getValue().asJsonObject().getString("value");
                    }
                }
            }
        }
        return "";
    }

    private static Object parseSnak(JsonValue object) {
        if (object instanceof JsonArray) {
            // Assume that it is likely a list of maps
            Object[] objects = ((JsonArray) object).stream().map(DataItem::parseSnak).toArray();
            if (objects.length == 1) {
                return objects[0];
            }
            return objects;
        } else if (object instanceof JsonObject) {
            String mainsnak = "mainsnak";
            String dataValue = "datavalue";
            JsonObject map = (JsonObject) object;
            if (map.containsKey(mainsnak) && "statement".equals(map.getString("type"))) {
                JsonObject mainsnakValue = map.getJsonObject(mainsnak);
                if (mainsnakValue.containsKey(dataValue)) {
                    JsonObject data = mainsnakValue.getJsonObject(dataValue);
                    // account for data["mainsnak"], data["datavalue"] (both should recurse)
                    if (data.containsKey("type")) {
                        String type = data.getString("type");
                        if ("wikibase-entityid".equals(type)) {
                            String tid = data.getJsonObject("value").getString("id");
                            if (WikiDataItem.fromValue(tid) != null) {
                                return WikiDataItem.fromValue(tid);
                            }
                            return tid;
                        } else if ("string".equals(type)) {
                            return data.getString("value");
                        } else if ("monolingualtext".equals(type)) {
                            return data.getJsonObject("value").getString("text");
                        }
                    }
                }
                Logging.info(map.toString());
            }
        }
        // OK. No clue. TODO do something else?
        return object;
    }

    /**
     * Ensure that properties are in the appropriate return format
     */
    private void updateProperties() {
        for (Map.Entry<WikiProperty, Object> entry : this.dataMap.entrySet()) {
            if (!entry.getKey().isString() && entry.getValue() instanceof String
                    && WikiDataItem.fromValue((String) entry.getValue()) != null) {
                entry.setValue(WikiDataItem.fromValue(((String) entry.getValue())));
            }
        }
        if (this.dataMap.containsKey(WikiProperty.VALUE_VALIDATION_REGEX_P13)) {
            String regex = (String) this.dataMap.get(WikiProperty.VALUE_VALIDATION_REGEX_P13);
            try {
                this.dataMap.put(WikiProperty.VALUE_VALIDATION_REGEX_P13, Pattern.compile("^(" + regex + ")$"));
            } catch (PatternSyntaxException e) {
                Logging.error(e);
            }
        }
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getDescriptor() {
        return this.baseName;
    }

    /**
     * Get the data map for this item
     * @return A map of Properties to values
     */
    public Map<WikiProperty, Object> getDataMap() {
        return this.dataMap;
    }

    /**
     * Get the label for this item
     * @return the label
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Get the description for this item
     * @return The description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Get the original JSON object for this item
     * @return The original JSON object
     */
    public JsonObject getOriginalJson() {
        return this.originalData;
    }
}
