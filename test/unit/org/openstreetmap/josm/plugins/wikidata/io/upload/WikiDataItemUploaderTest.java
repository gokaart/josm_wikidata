// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata.io.upload;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.openstreetmap.josm.plugins.wikidata.enums.WikiProperty;
import org.openstreetmap.josm.plugins.wikidata.io.download.WikiDataItemGetter;
import org.openstreetmap.josm.plugins.wikidata.items.DataItem;

class WikiDataItemUploaderTest {
    // For now, don't test actual upload
    @Test
    void testQueryPart() {
        // TODO switch to highway from cuisine=tex-mex (latter used due to not supporting more complex properties)
        DataItem highway = assertDoesNotThrow(() -> WikiDataItemGetter.getDataItem("cuisine", "tex-mex"));
        highway.getDataMap().put(WikiProperty.DESCRIPTION, "Test Description");
        String json = WikiDataItemUploader.queryPart(highway.getDataMap());
        assertEquals(
                "{\"labels\":{\"en\":{\"language\":\"en\",\"value\":\"cuisine=tex-mex\"}},\"descriptions\":{\"en\":{\"language\":\"en\",\"value\":\"Test Description\"}},\"sitelinks\":{\"wiki\":{\"site\":\"wiki\",\"title\":\"Tag:cuisine=tex-mex\"}},\"claims\":[{\"mainsnak\":{\"datavalue\":{\"value\":{\"entity-type\":\"item\",\"numeric-id\":\"2\",\"id\":\"Q2\"},\"type\":\"wikibase-entityid\"},\"property\":\"P2\",\"snaktype\":\"value\"},\"type\":\"statement\"},{\"mainsnak\":{\"datavalue\":{\"value\":{\"entity-type\":\"item\",\"numeric-id\":\"2\",\"id\":\"Q2\"},\"type\":\"wikibase-entityid\"},\"property\":\"P2\",\"snaktype\":\"value\"},\"type\":\"statement\"},{\"mainsnak\":{\"datavalue\":{\"type\":\"string\",\"value\":\"Q193\"},\"property\":\"P10\",\"snaktype\":\"value\"},\"type\":\"statement\"},{\"mainsnak\":{\"datavalue\":{\"type\":\"string\",\"value\":\"cuisine=tex-mex\"},\"property\":\"P19\",\"snaktype\":\"value\"},\"type\":\"statement\"},{\"mainsnak\":{\"datavalue\":{\"type\":\"string\",\"value\":\"CornmealProducts.jpg\"},\"property\":\"P28\",\"snaktype\":\"value\"},\"type\":\"statement\"},{\"mainsnak\":{\"datavalue\":{\"value\":{\"text\":\"Tag:cuisine=tex-mex\",\"language\":\"en\"},\"type\":\"monolingualtext\"},\"property\":\"P31\",\"snaktype\":\"value\"},\"type\":\"statement\"},{\"mainsnak\":{\"datavalue\":{\"value\":{\"entity-type\":\"item\",\"numeric-id\":\"8000\",\"id\":\"Q8000\"},\"type\":\"wikibase-entityid\"},\"property\":\"P33\",\"snaktype\":\"value\"},\"type\":\"statement\"},{\"mainsnak\":{\"datavalue\":{\"value\":{\"entity-type\":\"item\",\"numeric-id\":\"8001\",\"id\":\"Q8001\"},\"type\":\"wikibase-entityid\"},\"property\":\"P34\",\"snaktype\":\"value\"},\"type\":\"statement\"},{\"mainsnak\":{\"datavalue\":{\"value\":{\"entity-type\":\"item\",\"numeric-id\":\"8000\",\"id\":\"Q8000\"},\"type\":\"wikibase-entityid\"},\"property\":\"P35\",\"snaktype\":\"value\"},\"type\":\"statement\"},{\"mainsnak\":{\"datavalue\":{\"value\":{\"entity-type\":\"item\",\"numeric-id\":\"8001\",\"id\":\"Q8001\"},\"type\":\"wikibase-entityid\"},\"property\":\"P36\",\"snaktype\":\"value\"},\"type\":\"statement\"}]}",
                json);
    }
}
