// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata.download;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.openstreetmap.josm.plugins.wikidata.enums.WikiDataItem;
import org.openstreetmap.josm.plugins.wikidata.enums.WikiProperty;
import org.openstreetmap.josm.plugins.wikidata.io.download.WikiDataItemGetter;
import org.openstreetmap.josm.plugins.wikidata.items.DataItem;
import org.openstreetmap.josm.plugins.wikidata.items.NoSuchWikiDataItemException;

class WikiDataItemGetterTest {
    @Test
    void testKeyGetGood() {
        DataItem highway = assertDoesNotThrow(() -> WikiDataItemGetter.getDataItem("highway", null));
        assertEquals(WikiDataItem.KEY_Q7, highway.getDataMap().get(WikiProperty.INSTANCE_OF_P2));
        assertEquals("highway", highway.getDataMap().get(WikiProperty.PERMANENT_KEY_ID_P16));
    }

    @Test
    void testTagGetGood() {
        DataItem highwayResidential = assertDoesNotThrow(() -> WikiDataItemGetter.getDataItem("highway", "residential",
                WikiDataItemGetter.Options.FORCE_DOWNLOAD));
        assertEquals(WikiDataItem.TAG_Q2, highwayResidential.getDataMap().get(WikiProperty.INSTANCE_OF_P2));
        assertEquals("highway=residential", highwayResidential.getDataMap().get(WikiProperty.PERMANENT_TAG_ID_P19));
    }

    @Test
    void testAllNull() {
        assertThrows(NullPointerException.class, () -> WikiDataItemGetter.getDataItem(null, null));
    }

    @Test
    void testBadItem() {
        NoSuchWikiDataItemException exception = assertThrows(NoSuchWikiDataItemException.class,
                () -> WikiDataItemGetter.getDataItem("no-such-key", "no-such-value"));
        assertEquals("No WikiData for no-such-key=no-such-value", exception.getMessage());
    }
}
