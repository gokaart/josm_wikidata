// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata.enums;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

class WikiPropertyTest {

    @ParameterizedTest
    @EnumSource(WikiDataItem.class)
    void testUnique(WikiDataItem property) {
        assertTrue(Stream.of(WikiDataItem.values()).filter(p -> p != property)
                .noneMatch(p -> p.getId().equals(property.getId())));
    }

}
