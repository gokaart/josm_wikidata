// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.wikidata.enums;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.text.MessageFormat;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.openstreetmap.josm.TestUtils;

class WikiDataItemTest {
    private static final WikiProperty[] NON_STANDARD_PROPERTIES = { WikiProperty.ID, WikiProperty.DESCRIPTION };

    @ParameterizedTest
    @EnumSource(WikiProperty.class)
    void testUnique(WikiProperty property) {
        // Don't check non-property elements
        if (Stream.of(NON_STANDARD_PROPERTIES).anyMatch(property::equals)) {
            return;
        }
        Optional<WikiProperty> duplicateProperty = Stream.of(WikiProperty.values()).filter(p -> p != property)
                .filter(p -> p.getId().equals(property.getId())).findAny();
        assertFalse(duplicateProperty.isPresent(), MessageFormat.format("{0} ({2}) is not unique ({1})", property,
                duplicateProperty.orElse(null), property.getId()));
    }

    @Test
    void testEnumSuperficial() {
        assertDoesNotThrow(() -> TestUtils.superficialEnumCodeCoverage(WikiProperty.class));
    }
}
